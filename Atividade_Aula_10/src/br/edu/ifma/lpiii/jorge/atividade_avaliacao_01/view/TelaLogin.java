/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorAutenticacao;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorInicial;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.exception.UsuarioNaoEncontradoException;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.exception.UsuarioOuSenhaInvalidaException;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JButtonSair;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaLogin extends JFrame {

    private JTextField campoNickname;
    private JPasswordField campoSenha;

    public TelaLogin() {
        super("Login");
        this.setUp();
    }

    private void setUp() {

        JPanel painelForm = new JPanel();
        GridLayout layoutPanel = new GridLayout(3, 1);
        painelForm.setLayout(layoutPanel);

        JPanel painel0 = new JPanel();

        FlowLayout layoutLabels = new FlowLayout();
        layoutLabels.setAlignment(FlowLayout.CENTER);

        this.campoNickname = new JTextField("", 20);
        painel0.setLayout(layoutLabels);
        painel0.add(new JLabel("Login"));
        painel0.add(this.campoNickname);

        painelForm.add(painel0);

        JPanel painel1 = new JPanel();

        this.campoSenha = new JPasswordField("", 20);
        painel1.setLayout(layoutLabels);
        painel1.add(new JLabel("Senha"));
        painel1.add(this.campoSenha);

        painelForm.add(painel1);

        JPanel painelTopo = new JPanel();

        painelTopo.setLayout(new GridLayout(3, 1));

        JPanel topoTitulo = new JPanel();

        JLabel titulo = new JLabel("Login");
        titulo.setFont(new Font("Tahoma", Font.BOLD, 16));

        topoTitulo.add(titulo);

        JPanel topoBotao = new JPanel();

        JButton cadastrarUsuario = new JButton("Cadastrar Usuário");

        cadastrarUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaRegistroUsuario();
                dispose();
            }
        });

        topoBotao.add(cadastrarUsuario);

        painelTopo.add(topoTitulo);
        painelTopo.add(topoBotao);

        JPanel painelBotoes = new JPanel();

        JButton botaoLogin = new JButton("Entrar");
        JButtonSair botaoSair = new JButtonSair("Sair", this);

        botaoLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nickname = campoNickname.getText();
                String senha = campoSenha.getText();

                try {
                    if (ControladorAutenticacao.login(nickname, senha)) {
                        ControladorInicial.mostrarTelaInicial();
                        dispose();
                    }
                } catch (UsuarioNaoEncontradoException | UsuarioOuSenhaInvalidaException err) {
                    System.err.println(err);
                }
            }
        });

        painelBotoes.add(botaoLogin);
        painelBotoes.add(botaoSair);

        this.add(BorderLayout.NORTH, painelTopo);
        this.add(BorderLayout.CENTER, painelForm);
        this.add(BorderLayout.SOUTH, painelBotoes);

        this.setSize(600, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }
}
