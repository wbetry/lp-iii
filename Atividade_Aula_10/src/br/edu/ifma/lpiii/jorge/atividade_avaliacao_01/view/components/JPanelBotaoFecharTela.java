/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class JPanelBotaoFecharTela extends JPanel {

    public JPanelBotaoFecharTela(JFrame jframe) {
        JButtonSair botaoVoltar = new JButtonSair("Voltar", jframe);

        this.add(botaoVoltar);
    }

}
