/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components;

import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class JPanelPainelTitulo extends JPanel {

    public JPanelPainelTitulo(String titulo) {
        super();
        JLabelTitulo lblTitulo = new JLabelTitulo(titulo);
        
        this.add(lblTitulo);
    }
    
    
    
}
