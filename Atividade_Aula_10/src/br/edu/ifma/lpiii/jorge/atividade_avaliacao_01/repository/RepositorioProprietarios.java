/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.repository;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.exception.ProprietarioNaoEncontradoException;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Proprietario;
import java.util.ArrayList;

/**
 *
 * @author jorge
 */
public class RepositorioProprietarios {
    
    private ArrayList<Proprietario> proprietarios;

    public RepositorioProprietarios() {
        this.proprietarios = new ArrayList<>();
    }

    public void adicionarProprietario(Proprietario proprietario) {
        this.proprietarios.add(proprietario);
    }

    public Proprietario recuperarProprietario(int indice) {
        if (indice > proprietarios.size() - 1) {
            throw new ProprietarioNaoEncontradoException();
        }
        
        Proprietario proprietario = this.proprietarios.get(indice);

        return proprietario;
    }
    
}
