/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorAnimal;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorAutenticacao;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorProprietario;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JPanelPainelTitulo;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaInicial extends JFrame {

    public TelaInicial() {
        super("Tela Inicial");
        this.setUp();
    }

    private void setUp() {
        JPanelPainelTitulo painelTitulo = new JPanelPainelTitulo("Sistema de Cadastro de Animais");

        JPanel painelTela = new JPanel();

        painelTela.setLayout(new GridLayout(3, 1));

        JButton btnCadastroProprietario = new JButton("Cadastro de Proprietário");
        JButton btnCadastroAnimal = new JButton("Cadastro de animal");
        JButton btnAtribuiAnimalAProprietario = new JButton("Adicionar animal a proprietario");
        JButton btnConsultaAnimal = new JButton("Consulta de animal");
        JButton btnCadastroNovoUsuario = new JButton("Cadastro de Usuário");

        btnCadastroProprietario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorProprietario.mostrarTelaCadastroProprietario();
            }
        });

        btnCadastroAnimal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorAnimal.mostrarTelaCadastroAnimal();
            }
        });

        btnAtribuiAnimalAProprietario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorProprietario.mostrarTelaAdicionarAnimaisAProprietario();
            }
        });

        btnConsultaAnimal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorAnimal.mostrarTelaConsultaAnimal();
            }
        });

        btnCadastroNovoUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorAutenticacao.mostrarTelaRegistroUsuario();
                dispose();
            }
        });

        JPanel painelAnimal = new JPanel();
        painelAnimal.add(btnCadastroAnimal);
        painelAnimal.add(btnConsultaAnimal);

        JPanel painelProprietario = new JPanel();
        painelProprietario.add(btnCadastroProprietario);
        painelProprietario.add(btnAtribuiAnimalAProprietario);

        JPanel painelUsuario = new JPanel();
        painelUsuario.add(btnCadastroNovoUsuario);

        painelTela.add(painelAnimal);
        painelTela.add(painelProprietario);
        painelTela.add(painelUsuario);

        JPanel painelSair = new JPanel();

        JButton botaoSair = new JButton("Sair");

        botaoSair.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorAutenticacao.logout();

                new TelaLogin();
                dispose();
            }
        });

        painelSair.add(botaoSair);

        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.CENTER, painelTela);
        this.add(BorderLayout.SOUTH, painelSair);

        this.setSize(480, 360);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

}
