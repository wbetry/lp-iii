/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model;

/**
 *
 * @author jorge
 */
public class Usuario {
    
    private String nickname;
    
    private String senha;

    public Usuario(String nickname, String senha) {
        this.nickname = nickname;
        this.senha = senha;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSenha() {
        return senha;
    }
    
}
