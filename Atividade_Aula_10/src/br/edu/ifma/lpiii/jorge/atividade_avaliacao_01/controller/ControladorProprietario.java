/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Animal;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Proprietario;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.repository.RepositorioProprietarios;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.TelaAdicionarAnimaisAProprietario;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.TelaCadastroProprietario;

/**
 *
 * @author jorge
 */
public class ControladorProprietario {

    private static RepositorioProprietarios repositorio = new RepositorioProprietarios();

    public static void mostrarTelaCadastroProprietario() {
        new TelaCadastroProprietario();
    }

    public static void mostrarTelaAdicionarAnimaisAProprietario() {
        new TelaAdicionarAnimaisAProprietario();
    }

    public static void adicionarProprietario(Proprietario proprietario) {
        repositorio.adicionarProprietario(proprietario);
    }

    public static Proprietario recuperarProprietario(int indice) {
        return repositorio.recuperarProprietario(indice);
    }

    public static Proprietario adicionarAnimalAProprietario(int idProprietario, int idAnimal) {
        Proprietario proprietario = ControladorProprietario.recuperarProprietario(idProprietario);
        Animal animal = ControladorAnimal.recuperarAnimal(idAnimal);

        proprietario.adicionarAnimal(animal);

        return proprietario;
    }

}
