/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula04.tratamentodeeventos.exemplo1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author jorge
 */
public class PrintListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        Object fonte = e.getSource();
        System.out.println(e + " em " + fonte);
    }

}
