/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula04.tratamentodeeventos.exemplo2;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaV0 {

    private JFrame janela;
    private JLabel rotulo;
    private JButton botao;
    private JButton botaoDireita;

    public TelaV0() {
        this.setUp();
    }

    public void setUp() {
        this.janela = new JFrame("Janela Botao Circulo");
        this.janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        this.botao = new JButton("Altera Cor");
        ActionListener repintarTela = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                janela.repaint();
            }
        };
        this.botao.addActionListener(repintarTela);

        this.botaoDireita = new JButton("Botão - Alterar Label");
        ActionListener alterarLabel = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rotulo.setText("Novo nome no Rótulo");
            }
        };
        this.botaoDireita.addActionListener(alterarLabel);

        this.rotulo = new JLabel("Label");

        JPanel painel = new Painel();

        this.janela.add(BorderLayout.SOUTH, this.botao);
        this.janela.add(BorderLayout.CENTER, painel);
        this.janela.add(BorderLayout.EAST, botaoDireita);
        this.janela.add(BorderLayout.WEST, rotulo);

        this.janela.setSize(480, 300);
        this.janela.setVisible(true);
    }

}
