/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_08;

import java.util.ArrayList;

/**
 *
 * @author jorge
 */
public class RepositorioAnimais {

    private ArrayList<Animal> animais;

    public RepositorioAnimais() {
        this.animais = new ArrayList<>();
    }

    public void adicionarAnimal(Animal animal) {
        this.animais.add(animal);
    }

    public Animal recuperarAnimal(int indice) {
        Animal animal = this.animais.get(indice);

        return animal;
    }

}
