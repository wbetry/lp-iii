/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.unidade08;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author jorge
 */
final public class TelaBorderLayout extends JFrame {

    public TelaBorderLayout() {
        super("Tela BorderLayout");
        this.setUp();
    }

    private void setUp() {
        this.add(BorderLayout.CENTER, new JButton("Centro"));
        this.add(BorderLayout.NORTH, new JButton("Norte"));
        this.add(BorderLayout.SOUTH, new JButton("Sul"));
        this.add(BorderLayout.EAST, new JButton("Leste"));
        this.add(BorderLayout.WEST, new JButton("Oeste"));

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(480, 360);
        this.setVisible(true);
    }

}
