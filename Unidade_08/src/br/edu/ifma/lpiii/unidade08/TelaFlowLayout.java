/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.unidade08;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author jorge
 */
public class TelaFlowLayout extends JFrame {

    private JButton b1;
    private JButton b2;

    public TelaFlowLayout() {
        super("Tela FlowLayout");
        this.setUp();
    }

    private void setUp() {
        FlowLayout flow = new FlowLayout();

        flow.setAlignment(FlowLayout.RIGHT);

        this.setLayout(flow);

        this.b1 = new JButton("Botão 1");
        this.b2 = new JButton("Botão 2");

        this.add(this.b1);
        this.add(this.b2);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(480, 360);
        this.setVisible(true);
    }

}
