/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.unidade08;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author jorge
 */
final public class TelaSemGerenciador extends JFrame {

    private JButton b1;
    private JButton b2;
    private JButton b3;

    public TelaSemGerenciador() {
        super("Sem Gerenciador");
        this.setUp();
    }

    public void setUp() {
        this.setLayout(null);
        this.b1 = new JButton("Botao 1");
        this.b2 = new JButton("Botao 2");
        this.b3 = new JButton("Botao 3");

        this.b1.setBounds(10, 10, 150, 40);
        this.b2.setBounds(10, 60, 150, 40);
        this.b3.setBounds(10, 110, 150, 40);

        this.add(this.b1);
        this.add(this.b2);
        this.add(this.b3);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(480, 360);
        this.setVisible(true);
    }

}
