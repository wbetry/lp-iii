/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpii.arquivos.estruturado;

import java.util.StringTokenizer;

/**
 *
 * @author jorge
 */
public class TokenTest {
    
    public static void main(String[] args) {
        String str = "a;123;xyz";
        
        StringTokenizer tokenizer = new StringTokenizer(str, ";");
        
        while (tokenizer.hasMoreElements()) {
            System.out.println(tokenizer.nextToken());
        }        
    }
    
}
