/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.BotaoSair;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteBotoesRodape;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteData;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteEndereco;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.model.Endereco;
import br.edu.ifma.lpiii.atividade_aula_11.model.Proprietario;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioProprietarios;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaCadastroProprietario extends JFrame {

    private JTextField campoNome;

    private JTextField campoCpf;

    private ComponenteEndereco componenteEndereco;

    private ComponenteData campoDataDeNascimento;

    private JTextField campoTelefone;

    public TelaCadastroProprietario() {
        super("Cadastro de Proprietário de Animal");
        this.setUp();
    }

    private void setUp() {
        RotuloTitulo titulo = new RotuloTitulo("Cadastro de Proprietario");

        JPanel painelForm = new JPanel();
        GridLayout layoutPanel = new GridLayout(6, 1);
        painelForm.setLayout(layoutPanel);

        this.campoNome = new JTextField("", 30);
        ComponenteRotuloECampo componenteNome
                = new ComponenteRotuloECampo("Nome", this.campoNome);

        this.campoCpf = new JTextField("", 20);
        ComponenteRotuloECampo componenteCpf
                = new ComponenteRotuloECampo("CPF", this.campoCpf);

        this.componenteEndereco = new ComponenteEndereco();

        this.campoDataDeNascimento = new ComponenteData();
        ComponenteRotuloECampo componenteDataDeNascimento
                = new ComponenteRotuloECampo("Data de Nascimento", this.campoDataDeNascimento);

        this.campoTelefone = new JTextField("", 20);
        ComponenteRotuloECampo componentetelefone
                = new ComponenteRotuloECampo("Telefone", this.campoTelefone);

        painelForm.add(componenteNome);
        painelForm.add(componenteCpf);
        painelForm.add(componenteDataDeNascimento);
        painelForm.add(componenteEndereco);
        painelForm.add(componentetelefone);

        JButton botaoNovo = new JButton("Salvar");
        BotaoSair botaoCancelar = new BotaoSair("Cancelar", this);

        botaoNovo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String nome = campoNome.getText();
                    String cpf = "" + Long.parseUnsignedLong(campoCpf.getText());
                    Endereco endereco = componenteEndereco.recuperarEndereco();
                    GregorianCalendar dataDeNascimento = campoDataDeNascimento.parseToGregorianCalendar();
                    String telefone = "" + Long.parseUnsignedLong(campoTelefone.getText());

                    Proprietario proprietario = new Proprietario(cpf, nome, endereco, dataDeNascimento, telefone);

                    new RepositorioProprietarios().adicionarProprietario(proprietario);

                    JOptionPane.showMessageDialog(botaoNovo, "Proprietário Cadastrado com sucesso");

                    dispose();
                } catch (Exception err) {
                    JOptionPane.showMessageDialog(botaoNovo, "Erro ao Cadastrar Proprietário");
                }
            }
        });

        ComponenteBotoesRodape painelBotoes = new ComponenteBotoesRodape(botaoNovo, botaoCancelar);

        this.add(BorderLayout.NORTH, titulo);
        this.add(BorderLayout.CENTER, painelForm);
        this.add(BorderLayout.SOUTH, painelBotoes);

        this.setSize(720, 440);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
