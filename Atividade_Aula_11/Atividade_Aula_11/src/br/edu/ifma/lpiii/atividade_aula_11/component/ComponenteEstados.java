/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.component;

import javax.swing.JComboBox;

/**
 *
 * @author jorge
 */
public class ComponenteEstados extends JComboBox<String> {

    public ComponenteEstados() {
        super();
        for (String string : this.fornecerEstados()) {
            this.addItem(string);
        }
    }

    public String getNomeEstado() {
        int indice = this.getSelectedIndex();

        return this.fornecerEstados()[indice];
    }

    private String[] fornecerEstados() {
        return new String[]{
            "Acre",
            "Alagoas",
            "Amapá",
            "Amazonas",
            "Bahia",
            "Ceará",
            "Distrito Federal",
            "Espírito Santo",
            "Goiás",
            "Maranhão",
            "Mato Grosso",
            "Mato Grosso do Sul",
            "Minas Gerais",
            "Pará",
            "Paraíba",
            "Paraná",
            "Pernambuco",
            "Piauí",
            "Rio de Janeiro",
            "Rio Grande do Norte",
            "Rio Grande do Sul",
            "Rondônia",
            "Roraima",
            "Santa Catarina",
            "São Paulo",
            "Sergipe",
            "Tocantins"
        };
    }

}
