/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.component;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class ComponenteVoltarTelaInicial extends JPanel {

    public ComponenteVoltarTelaInicial(JFrame jframe) {
        BotaoSair botaoVoltar = new BotaoSair("Voltar", jframe);

        this.add(botaoVoltar);
    }

    private void setUp() {

    }

}
