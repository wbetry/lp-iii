/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.repository;

import br.edu.ifma.lpiii.atividade_aula_11.exception.ProprietarioNaoEncontradoException;
import br.edu.ifma.lpiii.atividade_aula_11.model.Animal;
import br.edu.ifma.lpiii.atividade_aula_11.model.Endereco;
import br.edu.ifma.lpiii.atividade_aula_11.model.Proprietario;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author jorge
 */
public class RepositorioProprietarios {

    private HashMap<String, Proprietario> proprietarios;
    private static Integer ultimoIdInserido = 0;

    public RepositorioProprietarios() {
        this.proprietarios = lerArquivo("files/proprietarios.txt");
    }

    public void adicionarProprietario(Proprietario proprietario) {
        if (proprietarios.containsKey(proprietario.getCpf())) {
            throw new RuntimeException("Proprietário já existe");
        }

        proprietario.setId(++ultimoIdInserido);
        this.proprietarios.put(proprietario.getCpf(), proprietario);
        escreverArquivo("files/proprietarios.txt", proprietario);
    }

    public Proprietario recuperarPorNome(String nome) {
        Iterator<Proprietario> iter = this.proprietarios.values()
                .stream()
                .filter((p) -> {
                    return nome.equals(p.getNome());
                })
                .iterator();

        if (!iter.hasNext()) {
            throw new ProprietarioNaoEncontradoException();
        }

        Proprietario proprietario = iter.next();

        return proprietario;
    }

    public ArrayList<Proprietario> recuperarPorCidade(String cidade) {
        ArrayList<Proprietario> proprietariosCidade = new ArrayList<>();

        Iterator<Proprietario> iter = this.proprietarios
                .values()
                .stream()
                .filter((p) -> {
                    return cidade.equals(p.getEndereco().getCidade());
                })
                .iterator();

        if (!iter.hasNext()) {
            throw new ProprietarioNaoEncontradoException();
        }

        do {
            Proprietario proprietario = iter.next();
            proprietariosCidade.add(proprietario);
        } while (iter.hasNext());

        return proprietariosCidade;
    }

    public ArrayList<Animal> recuperarAnimaisPorCpf(String cpfDono) {
        return new RepositorioAnimais().recuperarPorCpfDoProprietario(cpfDono);
    }

    public Proprietario recuperarPorCpf(String cpf) {
        if (!this.proprietarios.containsKey(cpf)) {
            throw new ProprietarioNaoEncontradoException();
        }

        return this.proprietarios.get(cpf);
    }

    private static HashMap<String, Proprietario> lerArquivo(String path) {
        HashMap<String, Proprietario> objetos = new HashMap<>();

        try (Scanner arquivo = new Scanner(new File(path));) {
            while (arquivo.hasNext()) {
                String leitura = arquivo.nextLine();

                StringTokenizer token = new StringTokenizer(leitura, "::");

                Integer id = Integer.parseInt(token.nextToken());
                String cpf = token.nextToken();
                String nome = token.nextToken();
                String enderecoStr = token.nextToken();
                String cidade = token.nextToken();
                String estado = token.nextToken();
                Integer dia = Integer.parseInt(token.nextToken());
                Integer mes = Integer.parseInt(token.nextToken());
                Integer ano = Integer.parseInt(token.nextToken());
                Long telefone = Long.parseLong(token.nextToken());

                Endereco endereco = new Endereco(enderecoStr, cidade, estado);

                GregorianCalendar dataDeNascimento = new GregorianCalendar(
                        ano, mes, dia);

                Proprietario proprietario = new Proprietario(
                        cpf, nome, endereco, dataDeNascimento, telefone.toString());
                proprietario.setId(id);
                objetos.put(cpf, proprietario);

                ultimoIdInserido = id;
            }
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("Repositório de Proprietários não encontrado");
        }

        return objetos;
    }

    private static void escreverArquivo(String path, Proprietario obj) {
        try (FileWriter arquivo = new FileWriter(new File(path), true)) {
            try (BufferedWriter buffer = new BufferedWriter(arquivo)) {

                StringBuilder builder = new StringBuilder();

                builder.append(obj.getId()).append("::");
                builder.append(obj.getCpf()).append("::");
                builder.append(obj.getNome()).append("::");
                builder.append(obj.getEndereco().getEndereco()).append("::");
                builder.append(obj.getEndereco().getCidade()).append("::");
                builder.append(obj.getEndereco().getEstado()).append("::");
                builder.append(obj
                        .getDataDeNascimento()
                        .get(GregorianCalendar.DAY_OF_MONTH)).append("::");
                builder.append(obj
                        .getDataDeNascimento()
                        .get(GregorianCalendar.MONTH)).append("::");
                builder.append(obj
                        .getDataDeNascimento()
                        .get(GregorianCalendar.YEAR)).append("::");
                builder.append(obj.getTelefone());

                buffer.write(builder.toString());
                buffer.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException("Erro ao abrir repositório de Proprietários");
        }
    }

}
