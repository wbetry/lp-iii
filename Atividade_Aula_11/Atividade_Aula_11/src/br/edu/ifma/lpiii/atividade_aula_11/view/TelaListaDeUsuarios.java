/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import br.edu.ifma.lpiii.atividade_aula_11.model.Usuario;
import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author jorge
 */
public class TelaListaDeUsuarios extends JFrame {

    private ArrayList<Usuario> usuarios;

    public TelaListaDeUsuarios(ArrayList<Usuario> usuarios) {
        super("Usuários");

        this.usuarios = usuarios;

        String[][] dados = this.formatarLinhas(usuarios);

        this.setUp(dados);
    }

    private void setUp(String[][] tuplas) {
        Object[] campos = {
            "Id",
            "Nome",
            "Nome de Usuário",
            "É Administrador"
        };

        JTable tabela = new JTable(tuplas, campos);
        JScrollPane scrollPane = new JScrollPane(tabela);

        ComponenteVoltarTelaInicial fecharTela
                = new ComponenteVoltarTelaInicial(this);

        this.add(BorderLayout.CENTER, scrollPane);
        this.add(BorderLayout.SOUTH, fecharTela);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(480, 360);
        this.setVisible(true);
    }

    private String[][] formatarLinhas(ArrayList<Usuario> usuarios) {
        int tamanho = usuarios.size();

        String[][] dados = new String[tamanho][5];

        int i = 0;
        for (Usuario u : usuarios) {
            dados[i][0] = "" + u.getId();
            dados[i][1] = u.getNome();
            dados[i][2] = u.getNickname();
            dados[i][3] = u.isAdmin() ? "Sim" : "Não";
            i++;
        }

        return dados;
    }

}
