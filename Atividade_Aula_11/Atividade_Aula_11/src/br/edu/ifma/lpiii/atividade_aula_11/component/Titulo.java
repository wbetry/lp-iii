/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.component;

import java.awt.Font;
import javax.swing.JLabel;

/**
 *
 * @author jorge
 */
public class Titulo extends JLabel {

    public Titulo(String titulo) {
        super(titulo);
        this.setUp(titulo);
    }

    private void setUp(String titulo) {
        this.setFont(new Font("Tahoma", Font.BOLD, 16));
    }

}
