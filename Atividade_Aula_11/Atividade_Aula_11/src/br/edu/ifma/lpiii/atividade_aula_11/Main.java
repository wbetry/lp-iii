/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11;

import br.edu.ifma.lpiii.atividade_aula_11.controller.ControladorAutenticacao;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioUsuarios;
import br.edu.ifma.lpiii.atividade_aula_11.view.TelaCadastroUsuario;

/**
 *
 * @author jorge
 */
public class Main {

    public static void main(String[] args) {

        try {
            new RepositorioUsuarios().recuperarTodosUsuarios();
            new ControladorAutenticacao().mostrarTelaLogin();
        } catch(RuntimeException rex) {
            new TelaCadastroUsuario();
        }
    }

}
