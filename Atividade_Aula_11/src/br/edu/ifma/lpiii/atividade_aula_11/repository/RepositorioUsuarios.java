/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.repository;

import br.edu.ifma.lpiii.atividade_aula_11.exception.UsuarioNaoEncontradoException;
import br.edu.ifma.lpiii.atividade_aula_11.model.Usuario;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author jorge
 */
public class RepositorioUsuarios {

    private final HashMap<String, Usuario> usuarios;
    private static Integer ultimoIdInserido = 0;

    public RepositorioUsuarios() {
        this.usuarios = lerArquivo("files/usuarios.txt");
    }

    public void adicionarUsuario(Usuario usuario) {
        if (this.usuarios.containsKey(usuario.getNickname())) {
            throw new RuntimeException("Usuário já existe");
        }

        usuario.setId(++ultimoIdInserido);
        this.usuarios.put(usuario.getNickname(), usuario);
        escreverArquivo("files/usuarios.txt", usuario);
    }

    public void persistirEdicao(Collection<Usuario> usuarios) {
        atualizarArquivo("files/usuarios.txt");
        for (Usuario usuario : usuarios) {
            this.adicionarUsuario(usuario);
        }
    }

    public void removerUsuario(String nickname) {
        this.usuarios.remove(nickname);

        this.persistirEdicao(this.usuarios.values());
    }

    public ArrayList<Usuario> recuperarTodosUsuarios() {
        ArrayList<Usuario> todosUsuarios = new ArrayList<>();

        Iterator<Usuario> iter = this.usuarios.values().iterator();

        if (!iter.hasNext()) {
            throw new RuntimeException("Sem Usuários Cadastrados");
        }

        while (iter.hasNext()) {
            todosUsuarios.add(iter.next());
        }

        return todosUsuarios;
    }

    public Usuario recuperarPorNickname(String nickname) {
        if (!this.usuarios.containsKey(nickname)) {
            throw new UsuarioNaoEncontradoException();
        }

        return this.usuarios.get(nickname);
    }

    private static HashMap<String, Usuario> lerArquivo(String path) {
        HashMap<String, Usuario> objetos = new HashMap<>();

        try (Scanner arquivo = new Scanner(new File(path));) {
            while (arquivo.hasNext()) {
                String leitura = arquivo.nextLine();

                StringTokenizer token = new StringTokenizer(leitura, "::");
                Integer id = Integer.parseInt(token.nextToken());
                String nome = token.nextToken();
                String nickname = token.nextToken();
                String senha = token.nextToken();
                boolean isAdmin = Boolean.parseBoolean(token.nextToken());

                Usuario usuario = new Usuario(nome, nickname, senha);
                usuario.setId(id);
                usuario.setIsAdmin(isAdmin);
                objetos.put(nickname, usuario);

                ultimoIdInserido = id;
            }
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("Repositório de Usuários não encontrado");
        }

        return objetos;
    }

    private static void escreverArquivo(String path, Usuario obj) {
        try (FileWriter arquivo = new FileWriter(new File(path), true)) {
            try (BufferedWriter buffer = new BufferedWriter(arquivo)) {
                StringBuilder builder = new StringBuilder();

                builder.append(obj.getId()).append("::");
                builder.append(obj.getNome()).append("::");
                builder.append(obj.getNickname()).append("::");
                builder.append(obj.getSenha()).append("::");
                builder.append(obj.isAdmin());

                buffer.write(builder.toString());
                buffer.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException("Erro ao abrir repositório de Usuários");
        }
    }

    private static void atualizarArquivo(String path) {
        try (FileWriter arquivo = new FileWriter(new File(path), false)) {
            try (BufferedWriter buffer = new BufferedWriter(arquivo)) {
                StringBuilder builder = new StringBuilder();
                buffer.write(builder.toString());
            }
        } catch (IOException e) {
            throw new RuntimeException("Erro ao abrir repositório de Usuários");
        }
    }

}
