/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.model;

import br.edu.ifma.lpiii.atividade_aula_11.exception.AnimalNaoEncontradoException;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.stream.Stream;

/**
 *
 * @author jorge
 */
public class Proprietario {

    private Integer id;

    private String nome;

    private String cpf;

    private Endereco endereco;

    private GregorianCalendar dataDeNascimento;

    private String telefone;

    private HashSet<Animal> animais;

    public Proprietario(
            String cpf,
            String nome,
            Endereco endereco,
            GregorianCalendar dataDeNascimento,
            String telefone) {
        this();
        this.nome = nome;
        this.cpf = cpf;
        this.endereco = endereco;
        this.dataDeNascimento = dataDeNascimento;
        this.telefone = telefone;
    }

    public Proprietario() {
        this.animais = new HashSet<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public GregorianCalendar getDataDeNascimento() {
        return dataDeNascimento;
    }

    public String getStringDataDeNascimento() {
        return dataDeNascimento.get(GregorianCalendar.DAY_OF_MONTH)
                + "/" + dataDeNascimento.get(GregorianCalendar.MONTH)
                + "/" + dataDeNascimento.get(GregorianCalendar.YEAR);
    }

    public void setDataDeNascimento(GregorianCalendar dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void adicionarAnimal(Animal animal) {
        this.animais.add(animal);
    }

    public Animal recuperarAnimal(String nome) {
        Iterator<Animal> animalReduced = this
                .recuperarStreamDeAnimais()
                .filter((animal) -> {
                    return nome.equals(animal.getNome());
                })
                .iterator();

        if (!animalReduced.hasNext()) {
            throw new AnimalNaoEncontradoException();
        }

        Animal animal = animalReduced.next();

        return animal;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("( ");
        builder.append(this.nome).append(";");
        builder.append(this.cpf).append(";");
        builder.append(this.endereco).append(";");
        builder.append(this.telefone).append(";");

        builder.append(" )");

        return builder.toString();
    }

    public Stream<Animal> recuperarStreamDeAnimais() {
        return this.animais.stream().filter((animal) -> {
            return this.equals(animal.getProprietario());
        });
    }

}
