/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.model.Animal;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioAnimais;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaBuscaAnimal extends JFrame {

    private JTextField campoNome;
    private JTextField campoCpf;
    private JTextField campoNomeDono;

    public TelaBuscaAnimal() {
        super("Tela de Busca de Animal");
        this.setUp();
    }

    private void setUp() {
        JPanel painelTitulo = new RotuloTitulo("Buscar Animal");

        JPanel painel = new JPanel();
        GridLayout layoutPanel = new GridLayout(7, 1);
        painel.setLayout(layoutPanel);

        this.campoNome = new JTextField("", 30);
        ComponenteRotuloECampo componenteNome
                = new ComponenteRotuloECampo("Buscar Por Nome do Animal", this.campoNome, FlowLayout.CENTER);
        JButton botao1 = new JButton("Buscar");
        ComponenteRotuloECampo btn1 = new ComponenteRotuloECampo("", botao1, FlowLayout.CENTER);

        this.campoCpf = new JTextField("", 20);
        ComponenteRotuloECampo componenteCpf
                = new ComponenteRotuloECampo("Buscar Por CPF do Proprietário", this.campoCpf, FlowLayout.CENTER);
        JButton botao2 = new JButton("Buscar");
        ComponenteRotuloECampo btn2 = new ComponenteRotuloECampo("", botao2, FlowLayout.CENTER);

        this.campoNomeDono = new JTextField("", 20);
        ComponenteRotuloECampo componenteNomeDono
                = new ComponenteRotuloECampo("Buscar Por Nome do Proprietário", this.campoNomeDono, FlowLayout.CENTER);
        JButton botao3 = new JButton("Buscar");
        ComponenteRotuloECampo btn3 = new ComponenteRotuloECampo("", botao3, FlowLayout.CENTER);

        painel.add(componenteNome);
        painel.add(btn1);
        painel.add(componenteCpf);
        painel.add(btn2);
        painel.add(componenteNomeDono);
        painel.add(btn3);

        botao1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Animal animal = new RepositorioAnimais()
                            .recuperarPorNome(campoNome.getText());

                    new TelaDetalhesAnimal(animal);
                } catch (RuntimeException err) {
                    new TelaInformacaoNaoEncontrada("Animal não encontrado");
                }

                dispose();
            }
        });

        botao2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<Animal> animais
                            = new RepositorioAnimais().recuperarPorCpfDoProprietario(campoCpf.getText());
                    new TelaListaDeAnimais(animais);
                } catch (RuntimeException err) {
                    new TelaInformacaoNaoEncontrada("Proprietário sem registros de animais");
                }

                dispose();
            }
        });

        botao3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<Animal> animais
                            = new RepositorioAnimais().recuperarPorNomeDoProprietario(campoNomeDono.getText());
                    new TelaListaDeAnimais(animais);
                } catch (RuntimeException err) {
                    new TelaInformacaoNaoEncontrada("Proprietário sem registros de animais");
                }

                dispose();
            }
        });

        ComponenteVoltarTelaInicial painelVoltar = new ComponenteVoltarTelaInicial(this);

        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.CENTER, painel);
        this.add(BorderLayout.SOUTH, painelVoltar);

        this.setSize(480, 480);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
