/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteBotoesRodape;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.controller.ControladorAutenticacao;
import br.edu.ifma.lpiii.atividade_aula_11.controller.ControladorInicial;
import br.edu.ifma.lpiii.atividade_aula_11.model.Usuario;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaCadastroUsuario extends JFrame {

    private JTextField campoNome;
    private JTextField campoNickname;
    private JPasswordField campoSenha;
    private Boolean ehAdministrador;

    public TelaCadastroUsuario() {
        super("Registro de Usuário");
        this.ehAdministrador = false;
        this.setUp();
    }

    private void setUp() {
        RotuloTitulo titulo = new RotuloTitulo("Registro de Usuário");

        JPanel painelForm = new JPanel();
        GridLayout layoutPanel = new GridLayout(5, 1);
        painelForm.setLayout(layoutPanel);

        FlowLayout layoutLabels = new FlowLayout();
        layoutLabels.setAlignment(FlowLayout.LEFT);

        this.campoNome = new JTextField("", 30);
        ComponenteRotuloECampo componenteNome
                = new ComponenteRotuloECampo("Nome", this.campoNome);

        this.campoNickname = new JTextField("", 20);
        ComponenteRotuloECampo componenteNickname
                = new ComponenteRotuloECampo("Usuario", this.campoNickname);

        this.campoSenha = new JPasswordField("", 20);
        ComponenteRotuloECampo componenteSenha
                = new ComponenteRotuloECampo("Senha", this.campoSenha);

        JCheckBox campoAdmin = new JCheckBox();
        ComponenteRotuloECampo componenteAdministrador
                = new ComponenteRotuloECampo("É administrador", campoAdmin);

        campoAdmin.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                ehAdministrador = !ehAdministrador;
            }
        });

        painelForm.add(componenteNome);
        painelForm.add(componenteNickname);
        painelForm.add(componenteSenha);
        painelForm.add(componenteAdministrador);

        JButton botaoRegistro = new JButton("Registrar");
        JButton botaoSair = new JButton("Sair");

        botaoRegistro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nome = campoNome.getText();
                String nickname = campoNickname.getText();
                String senha = campoSenha.getText();

                Usuario usuario = new Usuario(nome, nickname, senha);
                usuario.setIsAdmin(ehAdministrador);

                ControladorAutenticacao controladorAutenticacao
                        = new ControladorAutenticacao();

                controladorAutenticacao.registrar(usuario);

                if (controladorAutenticacao.isLogado()) {
                    new ControladorInicial().mostrarTelaInicial();
                } else {
                    controladorAutenticacao.mostrarTelaLogin();
                }

                dispose();
            }
        });

        botaoSair.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorAutenticacao controladorAutenticacao
                        = new ControladorAutenticacao();

                if (!controladorAutenticacao.isLogado()) {
                    controladorAutenticacao.mostrarTelaLogin();
                }

                dispose();
            }
        });

        ComponenteBotoesRodape painelBotoes
                = new ComponenteBotoesRodape(botaoRegistro, botaoSair);

        this.add(BorderLayout.NORTH, titulo);
        this.add(BorderLayout.CENTER, painelForm);
        this.add(BorderLayout.SOUTH, painelBotoes);

        this.setSize(600, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
