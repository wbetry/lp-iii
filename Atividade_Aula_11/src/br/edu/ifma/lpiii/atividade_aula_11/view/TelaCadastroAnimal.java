/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.BotaoSair;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteBotoesRodape;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteData;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.exception.ProprietarioNaoEncontradoException;
import br.edu.ifma.lpiii.atividade_aula_11.model.Animal;
import br.edu.ifma.lpiii.atividade_aula_11.model.Proprietario;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioAnimais;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioProprietarios;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaCadastroAnimal extends JFrame {

    private JTextField campoCpfProprietario;
    
    private JTextField campoNome;

    private JTextField campoEspecie;

    private JTextField campoRaca;

    private ComponenteData campoDataDeNascimento;

    public TelaCadastroAnimal() {
        super("Cadastro de Animal");
        this.setUp();
    }

    private void setUp() {
        RotuloTitulo painelTitulo = new RotuloTitulo("Cadastro de Animal");

        JPanel painelForm = new JPanel();
        GridLayout layoutPanel = new GridLayout(5, 1);
        painelForm.setLayout(layoutPanel);

        this.campoCpfProprietario = new JTextField("", 20);
        ComponenteRotuloECampo componenteCpfProprietario
                = new ComponenteRotuloECampo("CPF do Proprietário", this.campoCpfProprietario);
        
        this.campoNome = new JTextField("", 30);
        ComponenteRotuloECampo componenteNome
                = new ComponenteRotuloECampo("Nome", this.campoNome);

        this.campoEspecie = new JTextField("", 20);
        ComponenteRotuloECampo componenteEspecie
                = new ComponenteRotuloECampo("Espécie", this.campoEspecie);

        this.campoRaca = new JTextField("", 20);
        ComponenteRotuloECampo componenteRaca
                = new ComponenteRotuloECampo("Raça", this.campoRaca);

        this.campoDataDeNascimento = new ComponenteData();
        ComponenteRotuloECampo componenteDataDeNascimento
                = new ComponenteRotuloECampo("Data de Nascimento", this.campoDataDeNascimento);
        
        painelForm.add(componenteCpfProprietario);
        painelForm.add(componenteNome);
        painelForm.add(componenteEspecie);
        painelForm.add(componenteRaca);
        painelForm.add(componenteDataDeNascimento);

        JButton botaoNovo = new JButton("Salvar");
        BotaoSair botaoCancelar = new BotaoSair("Cancelar", this);

        botaoNovo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Proprietario proprietario 
                            = new RepositorioProprietarios().recuperarPorCpf(campoCpfProprietario.getText());
                    String nome = campoNome.getText();
                    String especie = campoEspecie.getText();
                    String raca = campoRaca.getText();
                    GregorianCalendar dataNascimento = campoDataDeNascimento.parseToGregorianCalendar();

                    Animal animal = new Animal(proprietario, nome, especie, raca, dataNascimento);

                    new RepositorioAnimais().adicionarAnimal(animal);

                    JOptionPane.showMessageDialog(botaoNovo, "Animal Cadastrado com sucesso");
                    
                    dispose();
                } catch (ProprietarioNaoEncontradoException err) {
                    if (err instanceof  ProprietarioNaoEncontradoException) {
                        JOptionPane.showMessageDialog(botaoNovo, "Proprietário não encontrado");
                    }
                }
            }
        });

        ComponenteBotoesRodape painelBotoes = new ComponenteBotoesRodape(botaoNovo, botaoCancelar);

        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.CENTER, painelForm);
        this.add(BorderLayout.SOUTH, painelBotoes);

        this.setSize(600, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
