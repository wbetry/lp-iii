/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import br.edu.ifma.lpiii.atividade_aula_11.model.Proprietario;
import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author jorge
 */
public class TelaListaDeProprietarios extends JFrame {

    private ArrayList<Proprietario> proprietarios;

    public TelaListaDeProprietarios(ArrayList<Proprietario> proprietarios) {
        super("Proprietarios");

        this.proprietarios = proprietarios;

        String[][] dados = this.formatarLinhas(proprietarios);

        this.setUp(dados);
    }

    private void setUp(String[][] tuplas) {
        Object[] campos = {
            "Id",
            "Nome",
            "CPF",
            "Data de Nascimento",
            "Endereço",
            "Cidade",
            "Estado"
        };

        JTable tabela = new JTable(tuplas, campos);
        JScrollPane scrollPane = new JScrollPane(tabela);

        ComponenteVoltarTelaInicial fecharTela
                = new ComponenteVoltarTelaInicial(this);

        this.add(BorderLayout.CENTER, scrollPane);
        this.add(BorderLayout.SOUTH, fecharTela);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(540, 360);
        this.setVisible(true);
    }

    private String[][] formatarLinhas(ArrayList<Proprietario> proprietarios) {
        int tamanho = proprietarios.size();

        String[][] dados = new String[tamanho][5];

        int i = 0;
        for (Proprietario p : proprietarios) {
            dados[i][0] = "" + p.getId();
            dados[i][1] = p.getNome();
            dados[i][2] = p.getCpf();
            dados[i][3] = p.getStringDataDeNascimento();
            dados[i][4] = p.getEndereco().getEndereco();
            dados[i][5] = p.getEndereco().getCidade();
            dados[i][6] = p.getEndereco().getEstado();
            i++;
        }

        return dados;
    }

}
