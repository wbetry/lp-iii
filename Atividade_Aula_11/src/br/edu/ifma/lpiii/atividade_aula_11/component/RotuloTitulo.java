/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.component;

import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class RotuloTitulo extends JPanel {

    public RotuloTitulo(String titulo) {
        super();
        this.setUp(titulo);
    }

    private void setUp(String titulo) {
        this.add(new Titulo(titulo));
    }

}
