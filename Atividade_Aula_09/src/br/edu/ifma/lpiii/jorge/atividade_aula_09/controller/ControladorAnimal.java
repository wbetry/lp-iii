/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_09.controller;

import br.edu.ifma.lpiii.jorge.atividade_aula_09.model.Animal;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.model.Proprietario;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.repository.RepositorioAnimais;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.view.TelaBuscaAnimal;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.view.TelaCadastroAnimal;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.view.TelaDetalhesAnimal;

/**
 *
 * @author jorge
 */
public class ControladorAnimal {
    
    private static RepositorioAnimais repositorio = new RepositorioAnimais();
    
    public static void mostrarTelaCadastroAnimal() {
        new TelaCadastroAnimal();
    }
    
    public static void mostrarTelaConsultaAnimal() {
        new TelaBuscaAnimal();
    }
    
    public static void mostrarTelaDetalhesAnimal(int indice) {
        Animal animal = ControladorAnimal.recuperarAnimal(indice);
        
        new TelaDetalhesAnimal(animal);
    }
    
    public static void adicionarAnimal(Animal animal) {
        repositorio.adicionarAnimal(animal);
    }
    
    public static Animal recuperarAnimal(int indice) {
        return repositorio.recuperarAnimal(indice);
    }
    
}
