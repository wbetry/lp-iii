/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_09.view;

import br.edu.ifma.lpiii.jorge.atividade_aula_09.model.Animal;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.controller.ControladorAnimal;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.exception.AnimalNaoEncontradoException;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaBuscaAnimal extends JFrame {

    private JTextField campoIndice;
    private JButton botaoSubmissao;
    
    public TelaBuscaAnimal() {
        super("");
        this.setUp();
    }
    
    private void setUp() {
        this.campoIndice = new JTextField("", 10);
        this.botaoSubmissao = new JButton("Buscar");
        
        JLabel titulo = new JLabel("Digite o ídice do Animal");
        titulo.setFont(new Font("Tahoma", Font.BOLD, 16));

        JPanel painel = new JPanel();
        
        painel.add(this.campoIndice);
        painel.add(this.botaoSubmissao);
        
        JPanel painelVoltar = new JPanel();
        
        JButton botaoVoltar = new JButton("Voltar");
        
        botaoVoltar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        
        painelVoltar.add(botaoVoltar);
        
        this.add(BorderLayout.CENTER, painel);
        this.add(BorderLayout.NORTH, titulo);
        this.add(BorderLayout.SOUTH, painelVoltar);
        
        this.botaoSubmissao.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Animal animal;
                
                try {
                    animal = ControladorAnimal.recuperarAnimal(Integer.parseInt(campoIndice.getText()));
                    new TelaDetalhesAnimal(animal);
                } catch (AnimalNaoEncontradoException err) {
                    new TelaInformacaoNaoEncontrada("Animal não encontrado");
                }
                
                
                dispose();
            }
        });
        
        this.setSize(480, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }
    
}
