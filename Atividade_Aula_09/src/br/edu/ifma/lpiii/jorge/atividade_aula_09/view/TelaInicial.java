/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_09.view;

import br.edu.ifma.lpiii.jorge.atividade_aula_09.controller.ControladorAnimal;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.controller.ControladorProprietario;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaInicial extends JFrame {

    private JButton btnCadastroProprietario;
    private JButton btnCadastroAnimal;
    private JButton btnConsultaAnimal;
    private JButton btnAtribuiAnimalAProprietario;
    
    public TelaInicial() {
        super("Tela Inicial");
        this.setUp();
    }
    
    private void setUp() {
        JPanel painelTela = new JPanel();
        
        painelTela.setLayout(new FlowLayout());
        
        this.btnCadastroProprietario = new JButton("Cadastro de Proprietário");
        this.btnCadastroAnimal = new JButton("Cadastro de animal");
        this.btnAtribuiAnimalAProprietario = new JButton("Adicionar animal a proprietario");
        this.btnConsultaAnimal = new JButton("Consulta de animal");
        
        this.btnCadastroProprietario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorProprietario.mostrarTelaCadastroProprietario();
            }
        });
        
        this.btnCadastroAnimal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorAnimal.mostrarTelaCadastroAnimal();
            }
        });
        
        this.btnAtribuiAnimalAProprietario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorProprietario.mostrarTelaAdicionarAnimaisAProprietario();
            }
        });
        
        this.btnConsultaAnimal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ControladorAnimal.mostrarTelaConsultaAnimal();
            }
        });
        
        painelTela.add(this.btnCadastroProprietario);
        painelTela.add(this.btnCadastroAnimal);
        painelTela.add(this.btnAtribuiAnimalAProprietario);
        painelTela.add(this.btnConsultaAnimal);
        
        JLabel titulo = new JLabel("Sistema de Cadastro de Animais");
        titulo.setFont(new Font("Tahoma", Font.BOLD, 16));

        this.add(titulo);
        
        this.add(painelTela);
        
        this.setSize(480, 360);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    
}
