/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_09.view;

import br.edu.ifma.lpiii.jorge.atividade_aula_09.controller.ControladorProprietario;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.exception.AnimalNaoEncontradoException;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.exception.ProprietarioNaoEncontradoException;
import br.edu.ifma.lpiii.jorge.atividade_aula_09.model.Proprietario;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaAdicionarAnimaisAProprietario extends JFrame {

    private JTextField campoIdProprietario;
    private JTextField campoIdAnimal;

    public TelaAdicionarAnimaisAProprietario() {
        super("Adicionar Animal a Proprietario");
        this.setUp();

    }

    private void setUp() {

        JPanel painelForm = new JPanel();
        GridLayout layoutPanel = new GridLayout(2, 1);
        painelForm.setLayout(layoutPanel);

        JPanel painel0 = new JPanel();

        FlowLayout layoutLabels = new FlowLayout();
        layoutLabels.setAlignment(FlowLayout.LEFT);

        this.campoIdProprietario = new JTextField("", 20);
        painel0.setLayout(layoutLabels);
        painel0.add(new JLabel("ID Proprietário"));
        painel0.add(this.campoIdProprietario);

        painelForm.add(painel0);

        JPanel painel1 = new JPanel();

        this.campoIdAnimal = new JTextField("", 20);
        painel1.setLayout(layoutLabels);
        painel1.add(new JLabel("Id Animal"));
        painel1.add(this.campoIdAnimal);

        painelForm.add(painel1);

        JLabel titulo = new JLabel("Adicionar Animal a proprietário");
        titulo.setFont(new Font("Tahoma", Font.BOLD, 16));

        this.add(BorderLayout.NORTH, titulo);
        this.add(BorderLayout.CENTER, painelForm);

        JButton botaoSalvar = new JButton("Salvar");
        JButton botaoCancelar = new JButton("Cancelar");

        botaoSalvar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    int idProprietario = Integer.parseInt(campoIdProprietario.getText());
                    int idAnimal = Integer.parseInt(campoIdAnimal.getText());

                    Proprietario p = ControladorProprietario.adicionarAnimalAProprietario(idProprietario, idAnimal);
                    
                    System.out.println(p);
                } catch (ProprietarioNaoEncontradoException | AnimalNaoEncontradoException err) {
                    new TelaInformacaoNaoEncontrada("Proprietário ou animal não encontrado");
                }
                
                dispose();
            }
        });

        botaoCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        JPanel painelBotoes = new JPanel();

        painelBotoes.add(botaoSalvar);
        painelBotoes.add(botaoCancelar);

        this.add(BorderLayout.SOUTH, painelBotoes);

        this.setSize(600, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
