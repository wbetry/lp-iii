/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula02.questao02;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author jorge
 */
public class Jogo {

    public static void main(String[] args) throws InterruptedException {
        while (true) {
            System.out.println("Pedra, Papel, Tesoura! ... ");

            TimeUnit.MILLISECONDS.sleep(800);

            Coisa jogada1 = Jogo.instanciarCoisa();
            Coisa jogada2 = Jogo.instanciarCoisa();

            System.out.println("Jogador 1: " + jogada1);
            System.out.println("Jogador 2: " + jogada2);

            switch (jogada1.compareTo(jogada2)) {
                case 1:
                    System.out.println("Papel ganha de Pedra");
                    break;
                case 2:
                    System.out.println("Pedra ganha de Tesoura");
                    break;
                case 3:
                    System.out.println("Tesoura ganha de Papel");
                    break;
                default:
                    System.out.println("Empate");
                    break;
            }

            System.out.println("\n");

            TimeUnit.MILLISECONDS.sleep(500);
        }
    }
    
    private static Coisa instanciarCoisa() {
        Random aleatorio = new Random();
        int jokenpo = aleatorio.nextInt(3);
        return Jogo.instanciarCoisa(jokenpo);
    }

    private static Coisa instanciarCoisa(int numero) {
        switch (numero) {
            case 0:
                return new Pedra();
            case 1:
                return new Papel();
            case 2:
                return new Tesoura();
            default:
                return instanciarCoisa();
        }
    }
}
