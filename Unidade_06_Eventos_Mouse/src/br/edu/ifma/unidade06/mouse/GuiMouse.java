/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.unidade06.mouse;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author jorge
 */
public class GuiMouse extends JFrame {

    private JLabel label1;

    public GuiMouse() {
        super("Detecção de Eventos de Mouse");
        this.setUp();
    }

    public final void setUp() {
        this.label1 = new JLabel();
        this.add(this.label1);
        
        MouseMotionListener mml = new MouseMotionListenerGui();
        
        this.addMouseListener(new MouseListenerGui(mml));
        this.addMouseMotionListener(mml);

        this.setSize(300, 200);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    class MouseListenerGui implements MouseListener {

        private MouseMotionListener mml;
        
        private MouseListenerGui(MouseMotionListener mml) {
            this.mml = mml;
        }
       
        @Override
        public void mouseClicked(MouseEvent e) {
            label1.setText("Clicado em [" + e.getX() + ", " + e.getY() + "]");
            removeMouseMotionListener(mml);
        }

        @Override
        public void mousePressed(MouseEvent e) {
            label1.setText("Pressionado em [" + e.getX() + ", " + e.getY() + "]");
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            label1.setText("Solto em [" + e.getX() + ", " + e.getY() + "]");
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            label1.setText("Mouse dentro da Janela");
        }

        @Override
        public void mouseExited(MouseEvent e) {
            label1.setText("Mouse fora da janela");
        }

    }

    class MouseMotionListenerGui implements MouseMotionListener {

        @Override
        public void mouseDragged(MouseEvent e) {
            label1.setText("Pressionado e movido para [" + e.getX() + ", " + e.getY() + "]");
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            label1.setText("Movendo para [" + e.getX() + ", " + e.getY() + "]");
        }
    }

}
